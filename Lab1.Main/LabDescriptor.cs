﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzlowiek);
        
        public static Type ISub1 = typeof(IMezczyzna);
        public static Type Impl1 = typeof(Chlopak);
        
        public static Type ISub2 = typeof(IKobieta);
        public static Type Impl2 = typeof(Dziewczyna);
        
        
        public static string baseMethod = "PodajImie";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "RozmiarStopy";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Wymiary";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Funkcja_1";
        public static string collectionConsumerMethod = "Funkcja_2";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(Zielonyczlek);

        public static string otherCommonMethod = "PodajImie";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
