﻿using System;
using System.Collections.Generic;
using System.Collections;
using Lab1.Contract;
using Lab1.Implementation;


namespace Lab1.Main
{
    public class Program
    {
        public ICollection<ICzlowiek> Funkcja_1()
        {
            IList<ICzlowiek> lista = new List<ICzlowiek>();
            lista.Add(new Dziewczyna());
            lista.Add(new Chlopak());
            lista.Add(new Dziewczyna());
            lista.Add(new Chlopak());
            lista.Add(new Chlopak());
            return lista;
        }

        public void Funkcja_2(ICollection<ICzlowiek> lista)
        {
            foreach (var item in lista)
            {
                item.PodajImie();
            }
        }

        static void Main(string[] args)
        {
            ICzlowiek czlek = new Zielonyczlek();
            Console.WriteLine(czlek.PodajImie());
            IMezczyzna chlop = new Zielonyczlek();
            Console.WriteLine(chlop.PodajImie());
            IKosmita kosmita = new Zielonyczlek();
            Console.WriteLine(kosmita.PodajImie());

            Console.ReadKey();
        }
    }
}
